import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'fs-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent implements OnInit {
  @Input() title: ElementRef;
  @Input() body: ElementRef;
  @Input() botonTrue: ElementRef;
  @Input() botonFalse: ElementRef;
  constructor(public ActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
