import { Component, OnInit, AfterContentInit, EventEmitter } from '@angular/core';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';
import { Router } from '@angular/router';
import { User } from 'src/app/auth/interface/user';
import { RestaurantServiceService } from 'src/app/restaurant/services/restaurant-service.service';

@Component({
  selector: 'top-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterContentInit {
  logged = false;
  assignMesa = false;
  user: User;
  esAdmin = false;
  esSuperAdmin = false;

  constructor(private AuthService: AuthServiceService, private router: Router, private RestService: RestaurantServiceService) { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.AuthService.loginChange$.subscribe(result => {
      this.logged = result;
      if (this.logged === true) {
        this.AuthService.getMe().subscribe(r2 => {
          this.AuthService.user = r2;
          this.user = r2;
          if (this.user.role == 'ROLE_ADMIN') {
            this.esAdmin = true;
            this.router.navigate(['/restaurants/me']);
          }
          if (this.user.role == 'ROLE_SUPER_ADMIN') {
            this.esSuperAdmin = true;
            this.router.navigate(['/restaurants/super']);
          }
          if (this.user.role == 'ROLE_USER') {
            this.esAdmin = false;
            this.esSuperAdmin = false;
            this.router.navigate(['/restaurants/mesa-assign']);
          }
      });
      }

    });
    this.AuthService.assignMesa$.subscribe(result => {
      this.assignMesa = result;
    });
  }

  logoutButton() {
    this.esAdmin = false;
    this.esSuperAdmin = false;
    this.user.id_mesa = 0;
    if (this.user.role != 'ROLE_ADMIN') {
      this.user.id_restaurante = 0;
    }
    this.RestService.putMeUser(this.user).subscribe(
      result => {
        this.AuthService.logout();
        this.router.navigate(['/auth/login']);
      }
    );
  }
}
