import { TestBed, async, inject } from '@angular/core/testing';

import { IsNotInTableGuard } from './is-not-in-table.guard';

describe('IsNotInTableGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsNotInTableGuard]
    });
  });

  it('should ...', inject([IsNotInTableGuard], (guard: IsNotInTableGuard) => {
    expect(guard).toBeTruthy();
  }));
});
