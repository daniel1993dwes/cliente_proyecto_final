import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { AuthServiceService } from '../auth/services/auth-service.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IsNotInTableGuard implements CanActivate  {
  constructor(private authService: AuthServiceService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.isInATable().pipe(
      map(ok => {
        console.log(ok);
        if (!ok) {
          this.router.navigate(['/restaurants/mesa-assign']);
        }
        return ok;
      })
    );
  }
}
