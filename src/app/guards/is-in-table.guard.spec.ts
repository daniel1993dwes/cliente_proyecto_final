import { TestBed, async, inject } from '@angular/core/testing';

import { IsInTableGuard } from './is-in-table.guard';

describe('IsInTableGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsInTableGuard]
    });
  });

  it('should ...', inject([IsInTableGuard], (guard: IsInTableGuard) => {
    expect(guard).toBeTruthy();
  }));
});
