import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../interface/user';
import { Observable, of, from } from 'rxjs';
import { switchMap, catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  logged = false;
  loginChange$ = new EventEmitter<boolean>();
  user: User;
  mesaAssigned = false;
  assignMesa$ = new EventEmitter<boolean>();

  constructor(private http: HttpClient, private router: Router) { }

  login(nombre: string, password: string): Observable<void> {
    return this.http.post<any>('auth/login', {nombre, password}).pipe(
      switchMap(async r => { // switchMap must return a Promise or observable (a Promise in this case)
        try {
          await localStorage.setItem('token', r.accessToken);
          this.setLogged(true);
        } catch (e) {
          throw new Error('Can\'t save authentication token in storage!');
        }
      })
    );
  }

  private setLogged(logged: boolean) {
    this.logged = logged;
    this.loginChange$.emit(logged);
  }

  setMesa(mesa: boolean) {
    this.mesaAssigned = mesa;
    this.assignMesa$.emit(this.mesaAssigned);
  }

  // loginGoogle(user: User): Observable<void> {
  //   return this.http.post<{lat: number, lng: number, accessToken: string}>
  //   ('auth/google', {lat: user.lat, lng: user.lng, token: user.token}).pipe(
  //     map(resp => {
  //       localStorage.setItem('token', resp.accessToken);
  //       this.logged = true;
  //       this.$loginChange.emit(this.logged);
  //     }), catchError((error: HttpErrorResponse) =>
  //     throwError(`Error Login. Status ${error.status}. Message: ${error.message}`)
  //     )
  //   );
  // }

  // loginFacebook(user: User): Observable<void> {
  //   return this.http.post<{lat: number, lng: number, accessToken: string}>
  //   ('auth/facebook', {lat: user.lat, lng: user.lng, token: user.token}).pipe(
  //     map(resp => {
  //       localStorage.setItem('token', resp.accessToken);
  //       this.logged = true;
  //       this.$loginChange.emit(this.logged);
  //     }), catchError((error: HttpErrorResponse) =>
  //     throwError(`Error Login. Status ${error.status}. Message: ${error.message}`)
  //     )
  //   );
  // }

  isLogged(): Observable<boolean> {
    if (this.logged === true) {
      return of(true);
    } else {
      const token = localStorage.getItem('token');
      if (this.logged === false && token === null) {
        return of(false);
      } else {
        return this.http.get<boolean>('auth/validate')
        .pipe(
          map(r => {
            this.setLogged(true);
            return true;
          }), catchError(error => {
            return of(false);
          })
        );
      }
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.logged = false;
    this.loginChange$.emit(this.logged);
    this.mesaAssigned = false;
    this.assignMesa$.emit(this.mesaAssigned);
  }

  register(user): Observable<void> {
    return this.http.post('auth/register', user).pipe(
      map(() => null)
    );
  }

  isInATable(): Observable<any> {
    if (this.mesaAssigned === true) {
      return of(true);
    } else {
      return this.http.get<any>('cuentas/not-assigned')
    .pipe(
      map(r => {
        if (r.result == 'ADMIN') {
          this.setMesa(true);
          this.router.navigate(['/restaurants/me']);
          return true;
        } else {
          this.setMesa(true);
          return true;
        }

      }), catchError(error => {
        return of(false);
      })
    );
    }
  }

  getUserByName(nombre: string) : Observable<any> {
    return this.http.post<any>('auth/nombre',{ nombre: nombre }).pipe(
      map(
        resp => {
          return resp;
        }), catchError(error => {
          return of(error);
        })
    );
  }

  getMe() : Observable<User> {
    return this.http.get<User>('users/me').pipe(
      map(
        resp => {
          return resp;
        }), catchError(error => {
          return of(error);
        })
    );
  }
}
