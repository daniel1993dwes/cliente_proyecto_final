import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interface/user';
import { NgModel, NgForm } from '@angular/forms';
import { AuthServiceService } from '../services/auth-service.service';
import { Title } from '@angular/platform-browser';
import { CanComponentDeactivate } from 'src/app/shared/interfaces/cancomponentDeactivate';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, from, of } from 'rxjs';
import { ModalWindowComponent } from 'src/app/shared/modal-window/modal-window.component';
import { switchMap, map, catchError } from 'rxjs/operators';

@Component({
  selector: 'fs-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, CanComponentDeactivate {
  nombre2 = '';
  errorGuardado = false;
  error = '';
  user: User;
  password_mesa: string;
  correosIguales = 'form-control mb-2';
  validoEspacios = false;

  @ViewChild('registerForm') registerForm: NgForm;
  @ViewChild('unElemento') nombre2Model: ElementRef;

  constructor(private router: Router,
              private UserService: AuthServiceService,
              private TitleService: Title,
              private ModalService: NgbModal) { }

  ngOnInit() {
    this.resetForm();
    this.TitleService.setTitle('Register | Easy Local');
    this.error = '';
  }

  cambiaCorreo() {
    if (this.user.nombre === this.nombre2 && this.user.nombre.split(' ').length == 1 && this.nombre2.split(' ').length == 1) {
      this.correosIguales = 'form-control mb-2 ng-touched ng-valid is-valid';
      this.nombre2Model.nativeElement.classList.remove('form-control', 'mb-2', 'ng-touched', 'ng-invalid', 'is-invalid');
      this.nombre2Model.nativeElement.classList.add('form-control', 'mb-2', 'ng-touched', 'ng-valid', 'is-valid');
      this.validoEspacios = true;
    } else {
      this.correosIguales = 'form-control mb-2 ng-touched ng-invalid is-invalid';
      this.nombre2Model.nativeElement.classList.remove('form-control', 'mb-2', 'ng-touched', 'ng-valid', 'is-valid');
      this.nombre2Model.nativeElement.classList.add('form-control', 'mb-2', 'ng-touched', 'ng-invalid', 'is-invalid');
      this.validoEspacios = false;
    }
  }

  buscaUsuario(nombre) {
    setTimeout(() => {
      this.UserService.getUserByName(nombre).subscribe(
        result => {
          if (result.length !== 0) {
            this.errorGuardado = true;
            this.error = 'Ese usuario ya existe';
          } else {
            this.errorGuardado = false;
            this.error = '';
          }
        }
      );
    }, 500);
  }

  mostrarEstado() {
    console.log(this.errorGuardado);
    console.log(this.validoEspacios);
  }

  canDeactivate(): Observable<boolean> {
    if (this.registerForm.dirty && this.registerForm.valid && !this.registerForm.submitted) {
      const modalRef = this.ModalService.open(ModalWindowComponent);
      modalRef.componentInstance.title = 'Save Register';
      modalRef.componentInstance.body = 'Do you want to save changes?';
      modalRef.componentInstance.botonTrue = 'Save changes';
      modalRef.componentInstance.botonFalse = 'Exit';

      return from(modalRef.result).pipe(
        switchMap(resp => {
          if (resp) {
            this.user.nombre = this.nombre2;
            return this.UserService.register(this.user).pipe(
              map(p => {
                return true;
              }),
              catchError(e => {
                console.log(e);
                this.errorGuardado = true;
                this.error = e.statusText;
                return of(false);
              })
            );
          } else {
            return of(true);
          }
        }),
        catchError(e => of(false))
      );
    }

    return of(true);
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }

  goToBack() {
    this.router.navigate(['/auth/login']);
  }

  resetForm() {
    this.password_mesa = '';
    this.user = {
      id_usuario: -1,
      id_restaurante: null,
      nombre: '',
      password: '',
      telefono: null,
      id_mesa: null,
      role: 'ROLE_USER'
    };
    this.nombre2 = '';
  }

  addUser() {
    if (this.registerForm.invalid) { return; }

    this.user.nombre = this.nombre2;
    this.UserService.register(this.user).subscribe(
        result => { this.router.navigate(['/auth/login']); },
        error => {
          this.errorGuardado = true;
        }
    );
  }

}
