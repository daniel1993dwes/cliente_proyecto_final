import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { User } from '../interface/user';

@Component({
  selector: 'fs-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  nombre: '';
  password: '';
  user: User;
  errorLogin = false;

  constructor(private TitleService: Title,
              private AuthService: AuthServiceService,
              private router: Router) { }

  addLogin() {
    this.AuthService.login(this.nombre, this.password).subscribe(
      res => {
      },
      error => {
        this.errorLogin = true;
      }
    );
  }

  goToRegister() {
    this.router.navigate(['/auth/register']);
  }

  ngOnInit() {
    this.TitleService.setTitle('Login | Easy Local');
    this.user = {
      id_usuario: -1,
      id_restaurante: -1,
      nombre: '',
      password: '',
      telefono: null,
      id_mesa: null,
      role: 'ROLE_USER'
    };
  }

}
