export interface User {
  id_usuario: number;
  id_restaurante: number;
  nombre: string;
  password: string;
  telefono: string;
  id_mesa: number;
  role: string;
}
