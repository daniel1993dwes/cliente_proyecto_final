import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActivateGuard } from './guards/logout-active.guard';
import { LoginActivateGuard } from './guards/login-active.guard';

const routes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule', canActivate: [LogoutActivateGuard]},
  { path: 'restaurants', loadChildren: './restaurant/restaurants.module#RestaurantModule'},
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
