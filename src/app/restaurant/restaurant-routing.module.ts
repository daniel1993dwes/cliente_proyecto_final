import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { MesasComponent } from './mesas/mesas.component';
import { RestaurantesComponent } from './restaurantes/restaurantes.component';
import { CuentasPorMesasComponent } from './cuentas-por-mesas/cuentas-por-mesas.component';
import { AssingTableComponent } from './assing-table/assing-table.component';
import { IsInTableGuard } from '../guards/is-in-table.guard';
import { LoginActivateGuard } from '../guards/login-active.guard';
import { IsNotInTableGuard } from '../guards/is-not-in-table.guard';
import { PagarCuentaComponent } from './pagar-cuenta/pagar-cuenta.component';
import { BusinessIntelligenceComponent } from './business-intelligence/business-intelligence.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurantsPageComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: 'mesas/:id',
    component: MesasComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: 'mesa-assign',
    component: AssingTableComponent,
    canActivate: [LoginActivateGuard, IsInTableGuard]
  },
  {
    path: 'intelligence',
    component: BusinessIntelligenceComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: 'me',
    component: RestaurantesComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: 'mesas/cuentas/:id',
    component: CuentasPorMesasComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: 'cuenta/pagar/:id',
    component: PagarCuentaComponent,
    canActivate: [LoginActivateGuard, IsNotInTableGuard]
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRoutingModule { }
