import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { Mesa } from '../interface/Mesa';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';
import { User } from 'src/app/auth/interface/user';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.scss']
})
export class MesasComponent implements OnInit {
  mesas: Mesa[];
  user: User;
  id: number;

  constructor(private router: ActivatedRoute,
              private RestService: RestaurantServiceService,
              private AuthService: AuthServiceService,
              private _location: Location,
              private TitleService: Title) { }

  ngOnInit() {
    this.TitleService.setTitle('Mesas | Easy Local');
    this.id = this.router.snapshot.params.id;
    this.RestService.getAllMesas(this.id).subscribe(
      result => {
        this.mesas = result;
      }
    );
    this.AuthService.getMe().subscribe(r2 => {
      this.user = r2;
  });
  }

  goBack() {
    this._location.back();
  }

  refrescar() {
    this.RestService.getAllMesas(this.id).subscribe(
      result => {
        this.mesas = result;
      }
    );
  }

}
