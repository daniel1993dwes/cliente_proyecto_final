import { Component, OnInit } from '@angular/core';
import { Mesa } from '../interface/Mesa';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';

@Component({
  selector: 'assing-table',
  templateUrl: './assing-table.component.html',
  styleUrls: ['./assing-table.component.scss']
})
export class AssingTableComponent implements OnInit {
  pass = '';
  errorMesa = false;
  mensajeError = '';

  constructor(private RestaurantService: RestaurantServiceService,
              private router: Router,
              private AuthService: AuthServiceService) { }

  ngOnInit() {
  }

  addMesa() {
    this.RestaurantService.putMeAccount(this.pass).subscribe(
      result => {
        this.AuthService.setMesa(true);
        this.errorMesa = false;
        this.router.navigate(['/restaurants']);
      },
      error => {
        console.log(error);
        this.errorMesa = true;
        this.mensajeError = error.mensajeError;
      }
    )
  }
}
