import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'pagar-cuenta',
  templateUrl: './pagar-cuenta.component.html',
  styleUrls: ['./pagar-cuenta.component.scss']
})
export class PagarCuentaComponent implements OnInit {

  constructor(private TitleService: Title) { }

  ngOnInit() {
    this.TitleService.setTitle('Ver Cuenta | Easy Local');
  }

}
