import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagarCuentaComponent } from './pagar-cuenta.component';

describe('PagarCuentaComponent', () => {
  let component: PagarCuentaComponent;
  let fixture: ComponentFixture<PagarCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagarCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagarCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
