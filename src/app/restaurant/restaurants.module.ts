import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantRoutingModule } from './restaurant-routing.module';
import { FormsModule } from '@angular/forms';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { SharedModule } from '../shared/shared.module';
import { MesasComponent } from './mesas/mesas.component';
import { RestaurantesComponent } from './restaurantes/restaurantes.component';
import { FiltrosRestaurantesPipe } from './pipes/filtros-restaurantes.pipe';
import { CuentasPorMesasComponent } from './cuentas-por-mesas/cuentas-por-mesas.component';
import { AssingTableComponent } from './assing-table/assing-table.component';
import { PagarCuentaComponent } from './pagar-cuenta/pagar-cuenta.component';
import { BusinessIntelligenceComponent } from './business-intelligence/business-intelligence.component';

@NgModule({
  declarations: [
    RestaurantsPageComponent,
    MesasComponent,
    RestaurantesComponent,
    FiltrosRestaurantesPipe,
    CuentasPorMesasComponent,
    AssingTableComponent,
    PagarCuentaComponent,
    BusinessIntelligenceComponent,
  ],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class RestaurantModule { }
