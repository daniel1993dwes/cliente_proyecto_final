import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Producto } from '../interface/Producto';
import { Pedido } from '../interface/Pedido';
import { User } from 'src/app/auth/interface/user';

@Injectable({
  providedIn: 'root'
})
export class RestaurantServiceService {
restURL = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getCuentaMe(): Observable<any> {
    return this.http.get<any>('cuentas/me')
    .pipe(
      map(resp => {
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getAllCuentasCerradas(id: number): Observable<any> {
    return this.http.get<any>('cuentas/restaurante/mesa/' + id)
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting closed accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getEstudioVentas(fecha1, fecha2): Observable<any> {
    return this.http.post<any>('estudios/ventas', {fecha1, fecha2})
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting closed accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getEstudioBfos(fecha1, fecha2): Observable<any> {
    return this.http.post<any>('estudios/ventas-stats', {fecha1, fecha2})
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting closed accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getEstudioProductos(fecha1, fecha2): Observable<any> {
    return this.http.post<any>('estudios/productos-stats', {fecha1, fecha2})
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting closed accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getMyRestaurant(): Observable<any> {
    return this.http.get<any>('restaurantes/me')
    .pipe(
      map(resp => {
        resp.result.imagen = this.restURL + '/' + resp.result.imagen;
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
        throwError(`Error posting Restaurants. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getAllMyRestaurants(): Observable<any> {
    return this.http.get<any>('restaurantes/')
    .pipe(
      map(resp => {
        return resp.result.map(r => {
          r.imagen = this.restURL + '/' + r.imagen;
          return r;
        })
      }), catchError((error: HttpErrorResponse) =>
        throwError(`Error posting Restaurants. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getAllOrders(id_cuenta: number): Observable<any> {
    return this.http.get<any>('pedidos/' + id_cuenta)
    .pipe(
      map(resp => {
         return resp.result.map(r => {
            r.imagen = this.restURL + '/' + r.imagen;
            return r;
        })
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Orders. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getAllProductos(): Observable<any> {
    return this.http.get<any>('productos')
    .pipe(
      map(resp => {
        return resp.result.map(pro => {
          pro.imagen = this.restURL + '/' + pro.imagen;
        return pro;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Products. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getProducto(id: number): Observable<any> {
    return this.http.get<any>('productos/' + id)
    .pipe(
      map(resp => {
        resp.result.imagen = this.restURL + '/' + resp.result.imagen;
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Product. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  putPedido(id: number, body: Producto): Observable<any> {
    return this.http.put<any>('pedidos/' + id, body)
    .pipe(
      map(resp => {
        resp.result.imagen = this.restURL + '/' + resp.result.imagen;
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error putting Orders. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  postPedido(pedido: Pedido) {
    return this.http.post<any>('pedidos/', pedido)
    .pipe(
      map(resp => {
        resp.result.imagen = this.restURL + '/' + resp.result.imagen;
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error posting Oders. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addCierre(id: number): Observable<any> {
    return this.http.get<any>('cuentas/cerrar/' + id)
    .pipe(
      map(resp => {
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error closing Accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addOpen(id: number): Observable<any> {
    return this.http.get<any>('cuentas/abrir/' + id)
    .pipe(
      map(resp => {
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error open Accounts. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  deletePedido(id_cuenta: number, id_order: number): Observable<any> {
    return this.http.delete<any>('pedidos/' + id_order + '/' + id_cuenta)
    .pipe(
      map(resp => {
        return resp;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Orders. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getAllMesas(id: number) {
    return this.http.get<any>('mesas/restaurant/' + id)
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Mesas. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  putMeAccount(pass: string) {
    return this.http.put<any>('cuentas/mesa', {password: pass})
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(error.error)
      )
    );
  }

  putMeUser(user: User) {
    return this.http.put<any>('users/me', user)
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(error.error)
      )
    );
  }

  getMesaId(id: number) {
    return this.http.get<any>('mesas/' + id)
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(error.error)
      )
    );
  }

  PagarUnaCuenta(id: number) {
    return this.http.get<any>('cuentas/pagar/' + id)
    .pipe(
      map(resp => {
        return resp.result;
      }), catchError((error: HttpErrorResponse) =>
      throwError(error.error)
      )
    );
  }
}
