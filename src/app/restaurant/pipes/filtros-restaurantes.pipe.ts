import { Pipe, PipeTransform } from '@angular/core';
import { Restaurant } from '../interface/Restaurant';

@Pipe({
  name: 'filtrosRestaurantes'
})
export class FiltrosRestaurantesPipe implements PipeTransform {

  transform(restaurantes: Restaurant[], search: string): any {
    let result = [];
    result = restaurantes;
    if (search !== '' ) {
      result = result.filter(e => {
          if (e.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 || e.descripcion.toLowerCase().indexOf(search.toLowerCase()) !== -1) {
              return e;
          }
      });
    }
    return result
  }

}
