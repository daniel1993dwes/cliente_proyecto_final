import { Component, OnInit, ElementRef, ViewChild, AfterContentInit } from '@angular/core';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { Title } from '@angular/platform-browser';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';
import { User } from 'src/app/auth/interface/user';
import { Cuenta } from '../interface/Cuenta';
import { Pedido } from '../interface/Pedido';
import { Restaurant } from '../interface/Restaurant';
import { Producto } from '../interface/Producto';

@Component({
  selector: 'fs-restaurants-page',
  templateUrl: './restaurants-page.component.html',
  styleUrls: ['./restaurants-page.component.scss']
})
export class RestaurantsPageComponent implements OnInit, AfterContentInit {
  user: User;
  cuenta: Cuenta;
  restaurante: Restaurant;
  pedidos: any[];
  pedido_muestra: Producto;
  cerrada = false;
  mensaje_cuenta: string;
  input: any;
  productos: Producto[];
  pedido: Pedido;

  @ViewChild('buttonCierre') buttonCierre: ElementRef;
  @ViewChild('buttonAbrir') buttonAbrir: ElementRef;

  constructor(private RestaurantService: RestaurantServiceService,
              private TitleService: Title,
              private AuthService: AuthServiceService) { }

  ngOnInit() {
    this.cerrada = false;
    this.TitleService.setTitle('Index | Easy Local');
  }

  ngAfterContentInit(): void {
    this.AuthService.getMe().subscribe(
      result => {
        this.user = result;
        this.RestaurantService.getMyRestaurant().subscribe(
          r1 => {
            this.restaurante = r1;
          }
        )
        this.RestaurantService.getAllProductos().subscribe(
          result => {
            let r_ch = result.map(r => {
              r.cantidad = 0;
              return r;
            });
            this.productos = r_ch;
          }
        )
        this.RestaurantService.getCuentaMe().subscribe(
          r1 => {
            this.cuenta = r1.result;
            if (this.cuenta.estado === 'CLOSED') {
              this.cerrada = true;
              this.buttonCierre.nativeElement.setAttribute("disabled", "");
              this.buttonAbrir.nativeElement.removeAttribute("disabled");
              this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
            } else {
              this.cerrada = false;
              this.buttonCierre.nativeElement.removeAttribute("disabled");
              this.buttonAbrir.nativeElement.setAttribute("disabled", "");
            }
            this.RestaurantService.getAllOrders(r1.result.id_cuenta).subscribe(
              r2 => {
                this.pedidos = r2;
                this.pedido_muestra = this.pedidos[0];
              }
            )
          }
        )
      }
    );
  }

  pedirProducto(producto: Producto) {
    this.pedido = {
      id_pedido: -1,
      id_cuenta: this.cuenta.id_cuenta,
      id_producto: producto.id_producto,
      cantidad: producto.cantidad,
      precio_final: null,
      fecha: null,
      coste_final: null,
      vat_final: null
    };
    this.RestaurantService.postPedido(this.pedido).subscribe(
      result => {
        producto.cantidad = 0;
        this.RestaurantService.getAllOrders(this.cuenta.id_cuenta).subscribe(
          r2 => {
            this.pedidos = r2;
            this.pedido_muestra = this.pedidos[0];
            this.RestaurantService.getCuentaMe().subscribe(
              r1 => {
                this.cuenta = r1.result;
              }
            )
          }
        )
      }
    );
  }

  recalcula(pedido: any, p: any) {
    pedido.cantidad = Number(p);
    let i = this.pedidos.indexOf(pedido);
    this.pedidos[i].precio_final = p * (pedido.precio_unit * (1 + pedido.vat));
    setTimeout(() => {
        this.RestaurantService.putPedido(pedido.id_pedido, pedido).subscribe(
          result => {
            this.RestaurantService.getCuentaMe().subscribe(
              r1 => {
                this.cuenta = r1.result;
                if (this.cuenta.estado === 'CLOSED') {
                  this.cerrada = true;
                  this.buttonCierre.nativeElement.setAttribute("disabled", "");
                  this.buttonAbrir.nativeElement.removeAttribute("disabled");
                  this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
                } else {
                  this.cerrada = false;
                  this.buttonCierre.nativeElement.removeAttribute("disabled");
                  this.buttonAbrir.nativeElement.setAttribute("disabled", "");
                }
              }
            );
          }
        );
      }, 1000);
  }

  cerrarCuenta(cuenta: Cuenta) {
    this.RestaurantService.addCierre(cuenta.id_cuenta).subscribe(
      result => {
        this.cerrada = true;
        this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
        this.buttonCierre.nativeElement.setAttribute("disabled", "");
        this.buttonAbrir.nativeElement.removeAttribute("disabled");
      },
      error => {
        this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
      }
    )
  }

  abrirCuenta(cuenta: Cuenta) {
    this.RestaurantService.addOpen(cuenta.id_cuenta).subscribe(
      result => {
        this.cerrada = false;
        this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
        this.buttonCierre.nativeElement.removeAttribute("disabled");
        this.buttonAbrir.nativeElement.setAttribute("disabled", "");
      },
      error => {
        this.mensaje_cuenta = 'Acercate a la barra para pagar la cuenta';
      }
    )
  }

  eliminarPedido(pedido: any) {
    this.RestaurantService.deletePedido(pedido.id_cuenta, pedido.id_pedido).subscribe(
      result => {
        var i = this.pedidos.indexOf(pedido);
        this.pedidos.splice( i, 1 );
        this.RestaurantService.getCuentaMe().subscribe(
          r1 => {
            this.cuenta = r1.result;
          }
        )
      }
    );
  }

  verProducto(item: number) {
    this.RestaurantService.getProducto(item).subscribe(
      r1 => {
        this.pedido_muestra = r1.result;
      }
    )
  }
  }

