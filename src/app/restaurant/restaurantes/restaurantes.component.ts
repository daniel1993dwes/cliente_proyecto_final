import { Component, OnInit } from '@angular/core';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';
import { Restaurant } from '../interface/Restaurant';
import { Mesa } from '../interface/Mesa';
import { User } from 'src/app/auth/interface/user';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.scss']
})
export class RestaurantesComponent implements OnInit {
  user: User;
  mesas: Mesa[];
  restaurantes: Restaurant[] = [];
  search = '';
  hay_restaurantes = false;
  mensaje_restaurante = '';

  constructor(private RestService: RestaurantServiceService,
              private AuthService: AuthServiceService,
              private TitleService: Title) { }

  ngOnInit() {
    this.TitleService.setTitle('Restaurantes | Easy Local');
    this.AuthService.getMe().subscribe(r2 => {
        this.user = r2;
    });
    this.RestService.getAllMyRestaurants().subscribe(
      result => {
        if (result.length == 0) {
          this.hay_restaurantes = false;
          this.mensaje_restaurante = 'No se han encontrado restaurantes para su usuario, contacte con el servico técnico para dar de alta su restaurante';
        } else {
          this.hay_restaurantes = true;
        }
        this.restaurantes = result;
      }
    );
  }

  searchRests() {
    setTimeout(() => {

    }, 1500);
  }

}
