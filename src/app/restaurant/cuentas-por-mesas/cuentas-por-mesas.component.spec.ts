import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentasPorMesasComponent } from './cuentas-por-mesas.component';

describe('CuentasPorMesasComponent', () => {
  let component: CuentasPorMesasComponent;
  let fixture: ComponentFixture<CuentasPorMesasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentasPorMesasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentasPorMesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
