import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { AuthServiceService } from 'src/app/auth/services/auth-service.service';
import { User } from 'src/app/auth/interface/user';
import { Restaurant } from '../interface/Restaurant';
import { Mesa } from '../interface/Mesa';
import { Location } from '@angular/common';
import { Cuenta } from '../interface/Cuenta';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'cuentas-por-mesas',
  templateUrl: './cuentas-por-mesas.component.html',
  styleUrls: ['./cuentas-por-mesas.component.scss']
})
export class CuentasPorMesasComponent implements AfterViewInit {
  id: number;
  cuentas: any[];
  user: User;
  rest: Restaurant;
  total_todas = 0;
  mesa: Mesa;
  hay_cuentas = false;
  mensaje_cuenta = '';
  devolucion = 0;
  pagado = '';

  constructor(private router: ActivatedRoute,
              private _location: Location,
              private RestService: RestaurantServiceService,
              private AuthService: AuthServiceService,
              private RouterNav: Router,
              private TitleService: Title) { }

  ngAfterViewInit() {
    this.TitleService.setTitle('Cuentas | Easy Local');
    this.id = this.router.snapshot.params.id;
    this.RestService.getAllCuentasCerradas(this.id).subscribe(
      result => {
        if (result.length === 0) {
          this.hay_cuentas = false;
          this.mensaje_cuenta = 'No se han encontrado cuentas para esa mesa';
        } else {
          this.hay_cuentas = true;
        }
        this.cuentas = result;
        this.cuentas.forEach(ele => {
          this.total_todas += ele.total_b;
        });
      }
    );
    this.RestService.getMyRestaurant().subscribe(
      result => {
        this.rest = result;
      }
    );
    this.AuthService.getMe().subscribe(r2 => {
      this.user = r2;
    });
    this.RestService.getMesaId(this.id).subscribe(
      result => {
        this.mesa = result;
      }
    );
  }

  calcular() {
    try {
      this.pagado = this.pagado.replace(',', '.');
    } catch {}
    this.devolucion = Number(this.pagado) - this.total_todas;
    this.devolucion = Number(this.devolucion.toFixed(2));
  }

  goBack() {
    this._location.back();
  }

  pagarUnaCuenta(cuenta: Cuenta) {
    console.log(cuenta);
    this.RestService.PagarUnaCuenta(cuenta.id_cuenta).subscribe(
      result => {
        console.log(result);
        this.refrescar();
      }
    )
  }

  pagarAllCuentas() {
    this.cuentas.forEach(cuenta => {
      this.RestService.PagarUnaCuenta(cuenta.id_cuenta).subscribe(
      result => {
        this.refrescar();
      }
    )
    })
  }

  verCuentaEnDetalle(cuenta: Cuenta) {
    this.RouterNav.navigate(['/cuenta/pagar/' + cuenta.id_cuenta]);
  }

  refrescar() {
    this.total_todas = 0;
    this.RestService.getAllCuentasCerradas(this.id).subscribe(
      result => {
        this.cuentas = result.result;
        if (this.cuentas.length !== 0) {
          this.cuentas.forEach(ele => {
            this.total_todas += ele.total;
          });
        } else {
          this.hay_cuentas = false;
          this.mensaje_cuenta = 'No se han encontrado cuentas para esa mesa';
        }
      }
    );
  }

}
