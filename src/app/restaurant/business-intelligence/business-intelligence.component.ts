import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { Title } from '@angular/platform-browser';
import { Cuenta } from '../interface/Cuenta';

@Component({
  selector: 'business-intelligence',
  templateUrl: './business-intelligence.component.html',
  styleUrls: ['./business-intelligence.component.scss']
})
export class BusinessIntelligenceComponent implements OnInit {
  tipoEstudio: string;
  mkestudio = false;
  fecha1: string;
  fecha2: string;
  cuentas: Cuenta[];
  mensajeError = '';
  error = false;
  datos = [];
  bfos = [];
  prods = [];
  muestraTabla = false;

  constructor(private RestService: RestaurantServiceService,
              private TitleServ: Title) { }

  ngOnInit() {
    this.TitleServ.setTitle('BI | Easy Local');
  }

  estudio(tipo: any) {
    this.muestraTabla = false;
    this.tipoEstudio = tipo;
    this.mkestudio = true;
  }

  mKStudioData() {
    if (this.tipoEstudio == 'VENTAS') {
      this.RestService.getEstudioVentas(this.fecha1, this.fecha2).subscribe(
        result => {
          this.cuentas = result;
          this.datos[0] = 0;
          this.datos[1] = 0;
          this.datos[2] = 0;
          this.cuentas.forEach(c => {
            this.datos[0] = Number(this.datos[0] + c.total_b);
            this.datos[1] = Number(this.datos[1] + c.total_c);
            this.datos[2] = Number(this.datos[2] + c.total_vat);
          });
          this.datos[0] = this.datos[0].toFixed(2);
          this.datos[1] = this.datos[1].toFixed(2);
          this.datos[2] = this.datos[2].toFixed(2);
        },
        error => {
          this.error = true;
          this.mensajeError = 'No se ha seleccionado ninguna cuenta';
        }
      )
      this.muestraTabla = true;
    }
    if (this.tipoEstudio == 'BFOS') {
      this.RestService.getEstudioBfos(this.fecha1, this.fecha2).subscribe(
        result => {
          this.bfos = result;
          this.datos[0] = 0;
          this.datos[1] = 0;
          this.datos[2] = 0;
          this.datos[3] = 0;
          this.bfos.forEach(c => {
            this.datos[0] = Number(this.datos[0] + c.total_b);
            this.datos[1] = Number(this.datos[1] + c.total_c);
            this.datos[2] = Number(this.datos[2] + c.total_vat);
            this.datos[3] = this.datos[3] + c.media_horas;
          });
          this.datos[0] = this.datos[0].toFixed(2);
          this.datos[1] = this.datos[1].toFixed(2);
          this.datos[2] = this.datos[2].toFixed(2);
        },
        error => {
          this.error = true;
          this.mensajeError = 'No se ha seleccionado ninguna cuenta';
        }
      )
      this.muestraTabla = true;
    }
    if (this.tipoEstudio == 'PR+') {
      this.RestService.getEstudioProductos(this.fecha1, this.fecha2).subscribe(
        result => {
          this.prods = result;
          this.prods = this.prods.sort((r1, r2) => {
            if (r1.total_a_cuenta > r2.total_a_cuenta) { return 1; }
            if (r1.total_a_cuenta < r2.total_a_cuenta) { return -1; }
            return 0;
          });
          this.datos[0] = 0;
          this.datos[1] = 0;
          this.prods.forEach(c => {
            this.datos[0] = Number(this.datos[0] + c.cnt_total);
            this.datos[1] = Number(this.datos[1] + c.total_a_cuenta);
          });
          this.datos[0] = this.datos[0].toFixed(2);
          this.datos[1] = this.datos[1].toFixed(2);
        },
        error => {
          this.error = true;
          this.mensajeError = 'No se ha seleccionado ninguna cuenta';
        }
      )
      this.muestraTabla = true;
    }
  }
}
