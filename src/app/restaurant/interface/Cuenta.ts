export interface Cuenta {
  id_cuenta: number;
  id_mesa: number;
  id_restaurante: number;
  id_usuario: number;
  total_b: number;
  total_c: number;
  total_vat: number;
  descripcion: string;
  fecha_apertura: Date;
  fecha_cierre: Date;
  estado: string;
}
