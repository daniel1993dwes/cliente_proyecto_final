export interface Restaurant {
  id_restaurante: number;
  id_usuario: number;
  nombre: string;
  telefono: string;
  imagen: string;
  direccion: string;
  descripccion: string;
  tipos_comida: string;
};
