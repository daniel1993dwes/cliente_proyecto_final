export interface Producto {
  id_producto: number;
  id_restaurante: number;
  nombre: string;
  imagen: string;
  precio_unit: number;
  descripcion: string;
  vat: number;
  coste: number;
  cantidad: number;
}
