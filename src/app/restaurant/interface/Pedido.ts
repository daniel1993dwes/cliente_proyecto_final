export interface Pedido {
  id_pedido: number;
  id_cuenta: number;
  id_producto: number;
  cantidad: number;
  precio_final: number;
  coste_final: number;
  vat_final: number;
  fecha: Date;
}
