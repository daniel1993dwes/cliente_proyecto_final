export interface Mesa {
  id_mesa: number;
  num_mesa: number;
  max_usuarios: number;
  cant_usuarios: number;
  password: string;
  ocupada: boolean;
  id_restaurante: number;
}
